@extends('layouts.app')

@section('content')

@include('reviews.form')

@include('reviews.filter')

<div class="container">
@foreach ($reviews as $review)
    <div class="card" style="display:flex;">
        <div class="card-content" style="flex:1 1 auto;">
            <div class="media">
                <div class="media-content">
                    <p class="title is-4">{{ $review->nickname }}</p>
                    <p class="subtitle is-6">
                        {{ $review->mail }}</br>
                        {{ $review->created_at->format('d-m-Y H:i:s') }}    
                    </p>
                </div>
                <div class="media-right">
                    <div class="rating align-right">
                    @foreach ($review->getRatingasArray() as $rating)
                        <div class="star {{ $rating ? 'is-selected' : '' }}">★</div>
                    @endforeach
                    </div>
                </div>
            </div>

            <div class="content">
                {!! $review->message !!}
            </div>
        </div>
        <div class="card-image" style="flex:0 0 200px;">
            @isset($review->file)
            <figure class="image is-square">
                <img src="/uploads/{{ $review->file }}" alt="" style="object-fit:contain;">
            </figure>
            @endisset
        </div>
    </div>
@endforeach
</div>

@endsection
