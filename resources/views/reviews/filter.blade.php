<div class="container" style="margin-bottom:1rem;">
    <form action="{{ action('ReviewsController@index') }}" method="get">
        <nav class="level">
            <div class="level-left">
                <div class="level-item">
                    <div class="field is-horizontal">
                        <div class="field-label is-normal" style="flex-grow:2;">
                            <label for="orderBy" class="label">Trier par</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <div class="select">
                                        <select name="orderBy" id="orderBy" onchange="this.form.submit()">
                                            <option value="">Défaut</option>
                                            @foreach ($orderByOptions as $optionKey => $optionValue)
                                                <option value="{{ $optionKey }}" {{ ($optionKey == $orderBySelected ? "selected" : "") }}>{{ $optionValue }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="level-item">
                    <div class="field is-horizontal">
                        <div class="field-label is-normal" style="flex-grow:2;">
                            <label for="filterRating" class="label">Notes</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <div class="select">
                                        <select name="filterRating" id="filterRating" onchange="this.form.submit()">
                                            @foreach ($filterRatingOptions as $optionKey => $optionValue)
                                                <option value="{{ $optionKey }}" {{ ($optionKey == $filterRatingSelected ? "selected" : "") }}>{{ $optionValue }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </form>
</div>