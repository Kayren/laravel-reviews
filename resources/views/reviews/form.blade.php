<div class="container" style="margin-bottom:2rem;">
    <form method="POST" action="{{ action('ReviewsController@create') }}" enctype="multipart/form-data">
        @csrf

        <div class="columns">
            <div class="column">
                <div class="field">
                    <label for="nicknane" class="label">Pseudo</label>
                    <div class="control">
                        <input id="nickname" name="nickname" type="text" class="input" maxlength="30" required>
                    </div>
                    @error('nickname')
                    <p class="help is-danger">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="column">
                <div class="field">
                    <label for="mail" class="label">Email</label>
                    <div class="control">
                        <input id="mail" name="mail" type="email" class="input" maxlength="30" required>
                    </div>
                    @error('mail')
                    <p class="help is-danger">{{ $message }}</p>
                    @enderror
                </div>
            </div>
        </div>

        <div class="field">
            <label for="message" class="label">Commentaire</label>
            <div class="control">
                <textarea id="message" name="message" class="textarea" placeholder="Textarea"></textarea>
            </div>
            @error('message')
            <p class="help is-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="columns">
            <div class="column">
        <!-- https://codepen.io/GeoffreyCrofte/pen/jEkBL -->
                <div class="field">
                    <label class="label">Notes<br/> <a href="#" onclick="resetRating(); return false;">Reset</a></label>
                    <div class="control rating">
                        <input id="rating5" name="rating" type="radio" value="5">
                        <label for="rating5">★</label>
                        <input id="rating4" name="rating" type="radio" value="4">
                        <label for="rating4">★</label>
                        <input id="rating3" name="rating" type="radio" value="3">
                        <label for="rating3">★</label>
                        <input id="rating2" name="rating" type="radio" value="2">
                        <label for="rating2">★</label>
                        <input id="rating1" name="rating" type="radio" value="1">
                        <label for="rating1">★</label>
                    </div>
                    @error('rating')
                    <p class="help is-danger">{{ $message }}</p>
                    @enderror
                </div>            
            </div>

            <div class="column">
                <div class="field">
                    <label for="" class="label">Fichier</label>
                    <div class="control">
                        <div class="file has-name">
                            <label for="file-picture" class="file-label">
                            <input type="file" id="file-picture" name="file" class="file-input" accept="image/png, image/jpeg, image/jpg">
                            <span class="file-cta">
                                <span class="file-label">Sélectionner un fichier</span>
                            </span>
                            <span class="file-name">
                                Aucun fichier choisi
                            </span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="field is-grouped">
            <div class="control">
                <button type="submit" class="button is-link">Submit</button>
            </div>
        </div>
    </form>
</div>

<script>
    function resetRating() {
        var ratingRadios = document.getElementsByName("rating");
        for (var i = 0; i < ratingRadios.length; i++)
            ratingRadios[i].checked = false;
    }

    $(document).ready(function() {
        $('#message').summernote({
            height: 120,
            toolbar: [
            ['style', []],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol']],
            ['table', []],
            ['insert', []],
            ['view', ['codeview', 'help']]
            ]
        });

        var fileInput = $('input[type=file]#file-picture')[0];
        console.log(fileInput);
        fileInput.onchange = function () {
            if (fileInput.files.length > 0) {
                var fileName = $('.file-name')[0];
                fileName.textContent = fileInput.files[0].name;
            }
        };
    });
</script>