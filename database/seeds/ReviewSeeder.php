<?php

use Illuminate\Database\Seeder;
use App\Review;

class ReviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Review::class, 30)->create()->each(function ($review) {
            $file = file_get_contents($review->file);
            $hash = hash('sha256',$file);
            $filename = time() . $hash;

            file_put_contents(public_path('uploads') . "/" . $filename, $file);

            $review->file = $filename;

            $review->save();
        });
    }
}