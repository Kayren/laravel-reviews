<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Review;
use Faker\Generator as Faker;

$factory->define(Review::class, function (Faker $faker) {
    return [
        'nickname' => $faker->name,
        'mail' => $faker->unique()->safeEmail,
        'message' => $faker->text(300),
        'rating' => $faker->numberBetween(0,5),
        'file' => $faker->imageUrl(640,480, 'cats', true)
    ];
});
