# Fizzup job interview test

A simple review form where a user can post a nice review or read reviews
already posted.

The list of reviews can be ordered by date or rating and can be filtered
by rating

## Get started 

first you need to create the .env file by copying it from the .env.example file 

```cp .env.example .env```

once this is done, you must edit it and fill in the fields `DB_ROOT_PASSWORD` and `DB_PASSWORD`.

### Docker

open a new terminal

For the Docker part, we must first create the docker image 

```docker-compose build```

One this command is finished we can launch the different containers

```docker-compose up```

now that the containers are active, let's move on to the application

### Laravel

Let's install the application dependencies with the following command

```docker exec -it fizzup-interview-test-app bash -c 'cd /srv/fizzup-interview-test && composer install'```

Generate the secret key

```docker exec -it fizzup-interview-test-app bash -c 'cd /srv/fizzup-interview-test && php artisan key:generate'```

it's almost finished, we just have to initialize the database and fill it with some data

### Database

Let's start with the tables

```docker exec -it fizzup-interview-test-app bash -c 'cd /srv/fizzup-interview-test && php artisan migrate'```

and now the data

```docker exec -it fizzup-interview-test-app bash -c 'cd /srv/fizzup-interview-test && php artisan db:seed --class=ReviewSeeder'```

## End

It's good, we hace finished whit this part. Now we just have to move on this app http://localhost:8080
