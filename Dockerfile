FROM php:7.4-fpm

ARG uid
ARG user

RUN apt-get update && apt-get install -y \
	git \
	curl \
	zip \
	unzip \
	libxml2-dev \
	libonig-dev \
	libpng-dev \
	zlib1g-dev \
	libzip-dev \
	libsodium-dev

RUN apt-get clean

RUN docker-php-ext-install pdo_mysql mbstring bcmath gd exif pcntl zip sodium

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

RUN useradd -G www-data -u $uid -d /srv/fizzup-interview-test $user

WORKDIR /home/$user

USER $user
