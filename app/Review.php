<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    public function getRatingAsArray() {
        $r = array_fill(0, 5, 0);
        $reviewRating = array_fill(0, $this->rating, 1);
        return array_replace($r, $reviewRating);
    }
}
