<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ReviewPost;
use App\Review;
use Mews\Purifier\Facades\Purifier;

class ReviewsController extends Controller
{
    public function index(Request $request) 
    {

        $reviews = Review::all();

        $orderByOptions = [
            "dateDesc" => "Les plus récents",
            "dateAsc" => "Les plus anciens",
            "ratingDesc" => "Les meilleures notes",
            "ratingAsc" => "Les pires notes"
        ];
        
        $orderBySelected = $request->query('orderBy');
        if (array_key_exists($orderBySelected, $orderByOptions)) {
            switch($orderBySelected) {
                case "dateAsc":
                    $reviews = $reviews->sortBy('created_at');
                    break;
                case "dateDesc":
                    $reviews = $reviews->sortByDesc('created_at');
                    break;
                case "ratingAsc":
                    $reviews = $reviews->sortBy('rating');
                    break;
                case "ratingDesc":
                    $reviews = $reviews->sortByDesc('rating');
                    break;
                default:
            }
        }

        $filterRatingoptions = [
            -1 => "Toutes les notes",
            0 => "0 ★",
            1 => "1 ★",
            2 => "2 ★",
            3 => "3 ★",
            4 => "4 ★",
            5 => "5 ★",
        ];

        $filterRatingSelected = $request->query('filterRating');
        $filterRatingInt = -1;
        if ($filterRatingSelected != null && is_numeric($filterRatingSelected)) {
            $filterRatingInt = intval($filterRatingSelected);
            if (0 <= $filterRatingInt && $filterRatingInt <= 5) {
                $reviews = $reviews->where('rating', $filterRatingInt);
            } else {
                $filterRatingInt = -1;
            }
        }

        return view('reviews.index', 
            [ 'reviews' => $reviews,
            'orderByOptions' => $orderByOptions,
            'orderBySelected' => $orderBySelected,
            'filterRatingOptions' => $filterRatingoptions,
            'filterRatingSelected' => $filterRatingInt
            ]
        );
    }

    public function create(ReviewPost $request) 
    {
        $data = $request->validated();
        $data['rating'] = $data['rating'] ?? 0;

        $review = new Review();

        if (array_key_exists('file', $data)) {
            $file = $data['file'];
            $hash = hash('sha256', $file);
            $filename = time() . $hash;

            $file->move(public_path('uploads'), $filename);
            $review->file = $filename;
        }

        $review->nickname = $data['nickname'];
        $review->mail = $data['mail'];
        $review->message = Purifier::clean($data['message']);
        $review->rating = $data['rating'];

        $review->save();
        
        return redirect()->route('reviews.index');
    }
    
}
