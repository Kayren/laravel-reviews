<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReviewPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        {
            return [
                'nickname' => 'required',
                'mail' => 'required|email',
                'message' => 'required',
                'rating' => 'integer',
                'file' => 'max:5000|mimes:jpeg,jpg,png'
            ];
        }
    }
}
